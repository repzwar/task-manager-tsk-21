package ru.pisarev.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
