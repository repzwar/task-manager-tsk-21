package ru.pisarev.tm.command.auth;

import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.util.TerminalUtil;

public class RegistryCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }
}
