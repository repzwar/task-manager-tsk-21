package ru.pisarev.tm.repository;

import ru.pisarev.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements ru.pisarev.tm.api.repository.IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return entities.values().stream()
                .filter(o -> login.equals(o.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return entities.values().stream()
                .filter(o -> email.equals(o.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user.getId());
        return user;
    }

}
