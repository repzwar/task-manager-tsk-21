package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(findAll(userId));
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findByName(final String userId, final String name) {
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName()))
                .findFirst().orElse(null);
    }

    @Override
    public Project findByIndex(final String userId, final int index) {
        List<Project> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project.getId());
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        entities.remove(project.getId());
        return project;
    }

    @Override
    public int getSize(final String userId) {
        List<Project> entities = findAll(userId);
        return entities.size();
    }

}
